Failure is the path to success. 
It helps us to touch the sky, teaches us to survive and shows us a specific way. 
Success brings in money, fame, pride and self-respect. Here it becomes very important to keep our head on out shoulder. 
The only way to show our gratitude to God for bestowing success on us is by being humble, modest, courteous and respectful to the less fortunate ones.
